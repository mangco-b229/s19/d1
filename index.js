

// [SECTION] - if, else if, else statement 

let numG = -1;

// if statement 
// executes a statement if a specified condition is true


if(numG < -5) { // will not execute because it did not met the condition
	console.log('Hello')
}	


if(numG < 1) { // this will execute since the condition was met or it is "true"
	console.log('Hello')
}	

// else if statement 

/*

1. Excutes a statement if previous cndition are false and if the specified condition is true
2. the "else if " clause is optional and can be added to capture additional condition

*/

let numH = 1

if(numG > 0) {
	console.log("Hello")
} else if (numH > 0) {
	console.log("World")
}

// else statement 
/*
1. executes a statement if all other conditions are  false
2. the "else" statement if optional and can be added to capture any other result to change the flow of the program
*/

if (numG > 0) {
	console.log("Hello")
} else if (numH == 0) {
	console.log("World")
} else {
	console.log("Again")
}

// if, else if, and else statement with functions


let message = "no message"

console.log(message)

function determineTyphoonItensity(windSpeed) {
	if(windSpeed < 30) {
		return "Not a Typhoon Yes."
	} else if (windSpeed <= 61) {
		return "Tropical Depression detected"
	} else if (windSpeed >= 62 && windSpeed <= 88) {
		return "Tropical Storm detected"
	} else if (windSpeed >= 89 && windSpeed <= 117) {
		return "Severe Tropical Storm Detected"
	} else {
		return "Typhoon Detected"
	}
}

//Return the string to the variable "message" that invoke it
message = determineTyphoonItensity(112)
console.log(message)

message = determineTyphoonItensity(70)
console.log(message)

if (message == "Tropical Storm detected" ) {
	console.warn
}


// [SECTION] - Truthy and flalsey

/*
In javascript a "trurthy" values ia values that is cosidered true when encountered in a boolean context

values are considered true unless defined otherwise

falsy values/exceptions for truthy:

1.false
2. 0
3. -0
4. ""
5. null
6. undefined
7. NaN
*/

if (true) {
	console.log("truthy")
}

if(1) {
	console.log("truthy")
}


if([]) {
	console.log("falsy")
}


if(false) {
	console.log("falsy")
}

// [SECTION] - Conditional (Ternary) Operator

let ternaryResult = (1 < 18) ? true : false

console.log("Result of ternary operator:" +ternaryResult)

let ternaryResult1 = (100 < 19) ? true : false

console.log("Result of ternary operator:" +ternaryResult1)

//multiple statement execution
//Both functions perform two separate tasks which changes the value of the "name" variable and returns

let name;


function isOfLegalAge() {
	name: "John"
	return "You are above the legal age limit";
}

function isUnderAge() {
	name = "Jane"
	return "You are under the age limit"
}

let age = parseInt(prompt("What is your age?"));

console.log(typeof age)
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge()
console.log(legalAge)


// [SECTION] - switch statement


let day = prompt("What day of the week is it today?").toLowerCase()

console.log(day)

switch (day) {
	case 'monday' :
		console.log("The color of the day is red")
		break;
	case 'tuesday':
		console.log("The color of the day is orange")
		break;
	case 'wednesday':
		console.log("The color of the day is yellow")
		break;
	case 'thursday':
		console.log("The color of the day is green")
		break;
	case 'friday':
		console.log("The color of the day is blue")
		break;
	case 'saturday':
		console.log("The color of the day is indigo")
		break;
	case 'sunday':
		console.log("The color of the day is violet")
		break;
	default:
		console.log("Please input a valid day")
}

// [SECTION] - try catch finally

function showIntensityAlert(windSpeed){
	try {
		alert(determineTyphoonItensity(windSpeed))
	} catch (err) {
		console.log(typeof err)
		console.warn(err.message)
	} finally {
		alert("Intensity updates will show new alert")
	}

}

showIntensityAlert(56)





